package dummydomain.yetanothercallblocker.sia.network;

import dummydomain.yetanothercallblocker.sia.utils.FileUtils;
import okhttp3.Request;
import okhttp3.Response;
import okio.Okio;
import okio.Sink;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import static java.util.Objects.requireNonNull;

public class DbDownloader {

    private static final Logger LOG = LoggerFactory.getLogger(DbDownloader.class);

    private final OkHttpClientFactory okHttpClientFactory;

    public DbDownloader(OkHttpClientFactory okHttpClientFactory) {
        this.okHttpClientFactory = okHttpClientFactory;
    }

    public boolean download(String url, String path) {
        LOG.info("download() started; path: {}", path);

        LOG.debug("download() making a request");
        Request request = new Request.Builder().url(url).build();
        DownloadResult result = download(path, request);

        if (result.success) return true;

        if (result.responseCode == 403) { // probably captcha
            LOG.debug("download() got 403, trying a workaround");

            // make the server happy with fake headers
            request = new Request.Builder()
                    .url(url)
                    .header("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8")
                    .header("User-Agent", "Mozilla/5.0 (Windows NT 10.0; rv:68.0) Gecko/20100101 Firefox/68.0")
                    .header("Accept-Encoding", "gzip, deflate, br") // may break things
                    .build();

            result = download(path, request);
        }

        return result.success;
    }

    private static class DownloadResult {
        boolean success;
        int responseCode;

        DownloadResult(boolean success, int responseCode) {
            this.success = success;
            this.responseCode = responseCode;
        }
    }

    private DownloadResult download(String path, Request request) {
        int responseCode = -1;

        try (Response response = okHttpClientFactory.getOkHttpClient().newCall(request).execute()) {
            if (response.isSuccessful()) {
                LOG.debug("download() got successful response");

                LOG.trace("download() processing zip");
                processZipStream(path, requireNonNull(response.body()).byteStream());
                LOG.trace("download() zip processed");

                LOG.debug("download() finished successfully");
                return new DownloadResult(true, response.code());
            } else {
                LOG.warn("download() unsuccessful response {}", response);
                responseCode = response.code();
            }
        } catch (IOException e) {
            LOG.warn("download()", e);
        }

        LOG.debug("download() finished unsuccessfully");
        return new DownloadResult(false, responseCode);
    }

    private void processZipStream(String path, InputStream inputStream) throws IOException {
        try (ZipInputStream zipInputStream = new ZipInputStream(inputStream)) {
            for (ZipEntry zipEntry; (zipEntry = zipInputStream.getNextEntry()) != null; ) {
                String name = zipEntry.getName();

                if (zipEntry.isDirectory()) {
                    FileUtils.createDirectory(path + name);
                    continue;
                }

                try (Sink out = Okio.sink(new File(path + name))) {
                    Okio.buffer(Okio.source(zipInputStream)).readAll(out);
                }
            }
        }
    }

}
